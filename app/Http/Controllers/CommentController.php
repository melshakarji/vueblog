<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use App\Post;

class CommentController extends Controller
{

    public function store(Request $request){

        $comment = new Comment;
        $comment->data = $request->get('body');
        $comment->user_id = request()->user()->id;
        $comment->post_id = $request->get('post');
        
        if ($comment->save()) {
            return response(['comment' => $comment->with("writtenBy")->where(["id" => $comment->id])->first()], 200);
        }
    }

    public function list(Post $post){
        $comments = Comment::where('post_id', $post->id)->with('writtenBy')->latest()->paginate(5);
        return $comments;
    }

    public function delete(Request $request){
        $comment = Comment::where('id', $request->comment_id)->first();
        if($request->user()->id == $comment->user_id || $request->user()->admin){
            $comment->delete();
            return response(['comment' => $comment], 200);
        }else{
            return response(['comment' => $comment], 500);
        }
    }
}
