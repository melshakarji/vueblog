<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Reaction;
use Auth;

class PostController extends Controller
{
    public function listPosts(Request $request) {
        $posts = Post::where('public', '1')->with('writtenBy')->latest()->get();
        if($request->user){
            $posts = Post::where('author', $request->user)->with('writtenBy')->latest()->get();
        }elseif($request->q){
            $posts = Post::where('public', '1')->OrderBy('likes', 'desc')->where('likes', '!=', 0)->with('writtenBy')->limit(3)->get();
        }

        return response(['posts' => $posts], 200);
    }

    public function store(Request $request) {
        $validated = $request->validate([
            'title' => 'required',
            'body' => 'required'
        ]);
        
        if($validated){
            $post = $request->edit_id ? Post::findOrFail($request->edit_id) : new Post;
            $edit = $request->edit_id ? true : false;
    
            $post->id = $request->input('edit_id');
            $post->title = $request->input('title');
            $post->body = $request->input('body');
            $post->author = $request->input('author');
            $post->likes = 0;
            $post->dislikes = 0;

            if($request->get('image'))
            {
                $image = $request->get('image');
                $name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
                \Image::make($request->get('image'))->save(public_path('images/').$name);
                $post->image_name = $name;
            }
           
        }

        if ($post->save()) {
            return response(['post' => $post, 'edit' => $edit], 200);
        }

    }

    public function show($id){
        $user = auth()->guard('api')->user();
        $post = Post::where('id', $id)->with('writtenBy','comments.writtenBy')->first();
        if($user){
            $post = Post::where('id', $id)->with('writtenBy', 'comments.writtenBy')->with(['reactions' => function ($query) use($user) {
                $query->where('user_id', $user->id)->first();
            }])->first();
            if(!$post->public && $user->id != $post->author){
                return response(['redirect' => true], 500);
            }
        }elseif(!$post->public){
            return response(['redirect' => true], 500);
        }
        return response(['post' => $post], 200);
    }

    public function react(Request $request){

        $add = $request->action;
        // find post 
        $post = Post::findOrFail($request->post_id);
        // find reaction
        $reaction = Reaction::where('subject', 'post')->where('subject_id', $request->post_id)->where('user_id', $request->user()->id)->first();
        if($reaction && !$add){
            $post->{$reaction->type.'s'} = $post->{$reaction->type.'s'}-1;
            $post->save();
            $reaction->delete(); 
        }else{
            // create reaction
            $reaction = new Reaction;
            $reaction->type = $request->reaction;
            $reaction->subject_id = $post->id;
            $reaction->subject = 'post';
            $reaction->user_id = $request->user()->id;
            $reaction->save();

            //update post
            $post->{$reaction->type.'s'} = $post->{$reaction->type.'s'}+1;
            $post->save();
        }
        return response('success', 200);
    }

    public function delete(Request $request){
        $post = Post::where('id', $request->post_id)->delete();
        return response(['post' => $post], 200);
    }
}
