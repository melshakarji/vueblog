<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Lcobucci\JWT\Parser as JwtParser;
use Laravel\Passport\TokenRepository;
use League\OAuth2\Server\AuthorizationServer;
use Psr\Http\Message\ServerRequestInterface;
use Laravel\Passport\Http\Controllers\AccessTokenController;

class TokenController extends AccessTokenController {
    public function __construct(AuthorizationServer $server, TokenRepository $tokens, JwtParser $jwt) {
        parent::__construct($server, $tokens, $jwt);
    }

    /**
    * User login
    * @param  [string] email
    * @param  [string] password
    * @return [string] json
    */
    public function login(ServerRequestInterface $request) {
        $data = $request->getParsedBody();

        $data['grant_type'] = 'password';
        $data['client_id'] = env("PASSPORT_CLIENT_ID");
        $data['client_secret'] = env("PASSPORT_CLIENT_SECRET");

        $data['username'] = (isset($data['email']) == true) ? $data['email'] : "";
        
        $token = parent::issueToken($request->withParsedBody($data));
        $token = json_decode($token->getContent(), true);

        if(isset($token["error"]) == true) {
            return response(['errors' => ["login" => ["The user credentials were incorrect."]]], 401);
        }

        $user = User::where('email', '=', $data['username'])->first();


        return response(["token" => $token, "user" => $user], 200);
    }

    public function logout(Request $request) {
        if($request->user()->token()->revoke() == true) {
            return response(["success" => "Successfully logged out"], 200);
        }

        return response(['errors' => ["logout" => ["Could not revoke authentication token."]]], 400);
    }
}