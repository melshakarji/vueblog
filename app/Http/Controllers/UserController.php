<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use App\Rules\MatchOldPassword;
use Illuminate\Validation\Rule;
use App\User;

class UserController extends Controller
{
    public function list()
    {
        $users = User::where('admin', 0)->get();
        return response(['users' => $users], 200);
    }

    public function store(Request $request)
    {

        $validated = $request->validate([
            'data.name' => 'required',
            'data.email' => 'required|email'
        ]);

        if($validated){
            $user = User::findOrFail($request->id);
            $user->name = $request->input('data.name');
            $user->email = $request->input('data.email');
            $user->save();
        }

        return response(['user' => $user], 200);
    }

    public function updateProfilePicture(Request $request)
    {
        $user = User::whereId($request->get('user'))->first();
        if($request->get('data'))
            {
                $image = $request->get('data');
                $name = time().'.' . explode('/', explode(':', substr($image, 0, strpos($image, ';')))[1])[1];
                if (!file_exists('images/'.$user->id)) {
                    mkdir('images/'.$user->id, 0777, true);
                }
                \Image::make($request->get('data'))->save(public_path('images/'.$user->id.'/').$name);
                $user->image_name = $name;
                $user->save();
                return response(['user' => $user], 200);
            }
        return response(['user' => $user], 500);
    }

    public function updatepass(Request $request)
    {
        $request->validate([
            'old_pass' => ['required', new MatchOldPassword],
            'new_pass' => 'required|different:old_pass',
            'confirm_pass' => 'same:new_pass',
        ]);
   
        $user = User::find(auth()->user()->id)->update(['password'=> Hash::make($request->new_pass)]);
        return response(['user' => $user], 200);
    }

    public function create(Request $request)
    {
        $emails = User::pluck('email')->toArray();
        $names = User::pluck('name')->toArray();

        
        
        $validated = $request->validate([
            'name' => 'required|notIn:'.implode(',',$names),
            'email' => 'required|email|notIn:'.implode(',',$emails),
            'password' => 'required',
            'repeat_password' => 'same:password'
        ]);

        if($validated){
            $user = New User;
            $user->name = $request->name;
            $user->email = $request->email;
            $user->password = Hash::make($request->password);
            $user->save();
            return response(['user' => $user], 200);
        }
    }

    public function delete(Request $request){
        $user = User::where('id', $request->user_id)->first();
        $user->posts()->delete();
        $user->delete();
        return response(['user' => $user], 200);
    }
}
