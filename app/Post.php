<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    protected $fillable = [
        'title', 'body', 'author',
    ];

    public function writtenBy()
    {
        return $this->belongsTo('App\User', 'author');
    }

    public function comments()
    {
        return $this->hasMany('App\Comment', 'post_id')->with('writtenBy');
    }

    public function reactions()
    {
        return $this->hasMany('App\Reaction', 'subject_id');
    }
}
