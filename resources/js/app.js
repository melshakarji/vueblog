/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

import router from "./router/router.js";
import store from "./store/store.js";
require('./bootstrap');

window.Vue = require('vue');

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i);
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default));

Vue.component('master', require('./components/Master.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */





// Check authentication for required routes.
router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (store.getters["auth/authenticated"] == true) {
            return next();
        }

        store.dispatch("auth/authenticate").then(() => {
            return next();
        }).catch(() => {
            return next("/login");
        });
    } else {

        if(store.getters["auth/data"].access_token != undefined && store.getters["auth/data"].access_token != "") {
            store.dispatch("auth/authenticate").then(() => {
                return next();
            }).catch(() => {
                return next("/login");
            });
        } else {
            return next();
        }
    }
});




const app = new Vue({
    el: "#app",
    router,
    store
});
