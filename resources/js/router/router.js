import Vue from "vue";
import VueRouter from "vue-router";

Vue.use(VueRouter);


import Home from "../components/pages/Home";
import Login from "../components/pages/Login";
import Profile from "../components/pages/Profile";
import Post from "../components/pages/Blogpost";
import Register from "../components/pages/Register";

import E404 from "../components/errors/404";



// Define Vue.js routes.
export default new VueRouter({
    mode: "history",
    base: process.env.BASE_URL,
    routes: [
        {
            path: "/",
            name: "home",
            component: Home
        },{
            path: "/login",
            name: "login",
            component: Login
        },{
            path: "/register",
            name: "register",
            component: Register
        },{
            path: "/profile",
            name: "profile",
            component: Profile,
            meta: {
                requiresAuth: true
            }
        },{
            path: "/post/:id",
            name: "post",
            component: Post
        },{
            path: "*",
            name: "404",
            component: E404
        }
    ]
});