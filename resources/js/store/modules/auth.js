import axios from 'axios';

export default {
    namespaced: true,
    state: {
        data: (localStorage.getItem("auth") != undefined && localStorage.getItem("auth") != "") ? JSON.parse(localStorage.getItem("auth")) : {
            access_token: "",
            refresh_token: "",
            token_type: "",
            expires_in: 0
        },
        authenticated: false
    },
    getters: {
        data(state){
            return state.data;
        },
        authenticated(state){
            return state.authenticated;
        }
    },
    mutations: {
        data(state, data){

            data = (data == null) ? {} : data;
            
            if(data.access_token != undefined && data.access_token != "") {
                Vue.prototype.$http.defaults.headers.common[
                    "Authorization"
                ] = "Bearer " + data.access_token;
            }

            localStorage.setItem("auth", JSON.stringify(data));
            state.data = data;
        },
        authenticated(state, authenticated){
            state.authenticated = authenticated;
        }
    },
    actions: {
        login({commit}, data){
            return new Promise((resolve, reject) => {
                axios({
                    url: "/login",
                    data: data,
                    method: "POST"
                }).then(response => {
                    const data = response.data != undefined ? response.data : {};
                    commit("data", data.token);
                    resolve(data);
                }).catch(error => {
                    reject(error);
                });
            });
        },
        authenticate({commit, getters}, data) {
            return new Promise((resolve, reject) => {
                if(getters["data"].access_token == "") {
                    reject(false);
                }

                axios({
                    url: "/api/user",
                    data: {},
                    method: "GET"
                }).then(response => {
                    const data = response.data != undefined ? response.data : {};
                    commit("authenticated", true);
                    commit("profile/data", data, {root: true});
                    resolve(data);
                }).catch(error => {
                    commit("data", null);
                    commit("authenticated", false);
                    commit("profile/data", {}, {root: true});
                    reject(true);
                });
            });
        },
        logout(context, data) {
            return new Promise((resolve, reject) => {
                axios({
                    url: "/logout",
                    data: {},
                    method: "POST"
                }).then(response => {
                    const data = response.data != undefined ? response.data : {};
                    context.commit("data", null);
                    context.commit("authenticated", false);
                    context.commit("admin", false);
                    resolve(data);
                }).catch(error => {
                    reject(error);
                });
            });
        },
        register(context, data){
            return new Promise((resolve, reject) => {
                axios({
                    url: "/register",
                    data: data,
                    method: "POST"
                }).then(response => {
                    const data = response.data != undefined ? response.data : {};
                    resolve(data);
                }).catch(error => {
                    reject(error);
                });
            });
        }
    }
};