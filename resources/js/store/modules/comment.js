import axios from 'axios';

export default {
    namespaced: true,
    state: {
        comments: []
    },
    getters: {
        all(state) {
            return state.comments;
        }
    },
    mutations: {
        all(state, data) {
            state.comments = data.reverse();
        },
        pageinfo(state, data) {
            state.pageinfo.data = data;
        },
        remove(state, data){
            state.comments = state.comments.filter(comment => comment.id !== data.comment_id)
        },
        push(state, data) {
            if(data.length !== undefined){
                data.forEach(comment => state.comments.unshift(comment));
            }else{
                console.log(state.comments.push(data));
            }
        }
    },
    actions: {
        fetch({commit}, data) {
            return new Promise((resolve, reject) => {
                axios({
                    url: "/api/comment/list/"+data,
                    data: data,
                    method: "GET"
                }).then(response => {
                    const data = response.data != undefined ? response.data : {};
                    commit("all", data.data);
                    resolve(data);
                }).catch(error => {
                    reject(error);
                });
            });
        },
        loadmore({commit}, url){
            return new Promise((resolve, reject) => {
                axios({
                    url: url,
                    method: "GET"
                }).then(response => {
                    const data = response.data != undefined ? response.data : {};
                    commit("push", data.data);
                    resolve(data);
                }).catch(error => {
                    reject(error);
                });
            });
        },
        add({commit}, data){
            return new Promise((resolve, reject) => {
                axios({
                    url: "/api/comment/add",
                    data: data,
                    method: "POST"
                }).then(response => {
                    const data = response.data != undefined ? response.data : {};
                    commit("push", data.comment);
                    resolve(data);
                }).catch(error => {
                    reject(error);
                });
            });
        },
        delete({commit}, id) {
            return new Promise((resolve, reject) => {
                axios({
                    url: "/api/deletecomment",
                    data: id,
                    method: "DELETE"
                }).then(response => {
                    const data = response.data != undefined ? response.data : {};
                    commit("remove", id);
                    resolve(data);
                }).catch(error => {
                    reject(error);
                });
            });
        }
    }
};