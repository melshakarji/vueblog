import axios from 'axios';
import router from '../../router/router';

export default {
    namespaced: true,
    state: {
        posts: [],
        popular: [],
    },
    getters: {
        all(state) {
            return state.posts;
        },
        popular(state) {
            return state.popular;
        },
        show(state, data){
            return state.data;
        }
    },
    mutations: {
        all(state, data) {
            state.posts = data;
        },
        popular(state, data) {
            state.popular = data;
        },
        push(state, data){
            state.posts.unshift(data);
        },
        update(state, data) {
            const item = state.posts.find(item => item.id === data.id);
            Object.assign(item, data);
        },
        show(state, data) {
            state.post = data;
        },
        remove(state, data){
            state.posts = state.posts.filter(post => post.id !== data.post_id)
        }
    },
    actions: {
        fetch({commit}, data) {
            if(data){
                var url = "/api/posts/all?q="+data.data;
                var after = "popular";
            }else{
                var url = "/api/posts/all";
                var after = "all";
            }

            return new Promise((resolve, reject) => {
                axios({
                    url: url,
                    data: {},
                    method: "GET"
                }).then(response => {
                    const data = response.data != undefined ? response.data : {};
                    commit(after, data.posts);
                    resolve(data);
                }).catch(error => {
                    reject(error);
                });
            });
        },
        fetchmine({commit}, data) {
            return new Promise((resolve, reject) => {
                axios({
                    url: "/api/posts/all/?user="+data.user,
                    data: data,
                    method: "GET"
                }).then(response => {
                    const data = response.data != undefined ? response.data : {};
                    commit("all", data.posts);
                    resolve(data);
                }).catch(error => {
                    reject(error);
                });
            });
        },
        fetchSinglePost({commit}, data) {
            return new Promise((resolve, reject) => {
                axios({
                    url: "/api/post/"+data,
                    data: data,
                    method: "GET"
                }).then(response => {
                    const data = response.data != undefined ? response.data : {};
                    resolve(data);
                }).catch(error => {
                    router.push('/');
                    reject(error);
                });
            });
        },
        add({commit}, data) {
            return new Promise((resolve, reject) => {
                axios({
                    url: "/api/addpost",
                    data: data,
                    method: "POST",
                }).then(response => {
                    const data = response.data != undefined ? response.data : {};
                    if(!data.edit){
                        commit("push", data.post);
                    }else{
                        commit("update", data.post);
                    }
                    resolve(data);
                }).catch(error => {
                    reject(error);
                });
            });
        },
        react({commit}, data){
            return new Promise((resolve, reject) => {
                axios({
                    url: "/api/post/react",
                    data: data,
                    method: "POST",
                }).then(response => {
                    const data = response.data != undefined ? response.data : {};
                    resolve(data);
                }).catch(error => {
                    reject(error);
                });
            });
        },
        delete({commit}, id) {
            return new Promise((resolve, reject) => {
                axios({
                    url: "/api/deletepost",
                    data: id,
                    method: "DELETE"
                }).then(response => {
                    const data = response.data != undefined ? response.data : {};
                    commit("remove", id);
                    resolve(data);
                }).catch(error => {
                    reject(error);
                });
            });
        }
    }
};