import axios from 'axios';

export default {
    namespaced: true,
    state: {
        data: {}
    },
    getters: {
        data(state){
            return state.data;
        },
        email(state) {
            return (state.data.email != undefined) ? state.data.email : "";
        },
        username(state) {
            return (state.data.name != undefined) ? state.data.name : "";
        },
        profile_picture(state) {
            return (state.data.image_name != undefined) ? state.data.image_name : "";
        },
        userid(state) {
            return (state.data.id != undefined) ? state.data.id : "";
        },
        admin(state) {
            return (state.data.admin != undefined) ? state.data.admin : false;
        }
    },
    mutations: {
        data(state, data){
            state.data = data;
        }
    },
    actions: {
        update({commit}, data){
            return new Promise((resolve, reject) => {
                axios({
                    url: "/api/update/user",
                    data: data,
                    method: "POST"
                }).then(response => {
                    const data = response.data != undefined ? response.data : {};
                    resolve(data);
                }).catch(error => {
                    reject(error);
                });
            });
        },
        updateImage({commit}, data){
            return new Promise((resolve, reject) => {
                axios({
                    url: "/api/update/image",
                    data: data,
                    method: "POST"
                }).then(response => {
                    const data = response.data != undefined ? response.data : {};
                    resolve(data);
                }).catch(error => {
                    reject(error);
                });
            });
        },
        password({commit}, data){
            return new Promise((resolve, reject) => {
                axios({
                    url: "/api/update/password",
                    data: data,
                    method: "POST"
                }).then(response => {
                    const data = response.data != undefined ? response.data : {};
                    resolve(data);
                }).catch(error => {
                    reject(error);
                });
            });
        }
    }
};