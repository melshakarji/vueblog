import axios from 'axios';

export default {
    namespaced: true,
    state: {
        data: {}
    },
    getters: {
        data(state){
            return state.data;
        },
        all(state){
            return state.data;
        }
    },
    mutations: {
        data(state, data) {
            state.data = data;
        },
        all(state, data) {
            state.data = data;
        },
        remove(state, data){
            state.data = state.data.filter(user => user.id !== data.user_id)
        }
    },
    actions: {
        fetch({commit}, data){
            return new Promise((resolve, reject) => {
                axios({
                    url: "/api/list/users",
                    method: "GET"
                }).then(response => {
                    const data = response.data != undefined ? response.data : {};
                    commit("all", data.users);
                    resolve(data);
                }).catch(error => {
                    reject(error);
                });
            });
        },
        delete({commit}, id) {
            return new Promise((resolve, reject) => {
                axios({
                    url: "/api/deleteuser",
                    data: id,
                    method: "DELETE"
                }).then(response => {
                    const data = response.data != undefined ? response.data : {};
                    commit("remove", id);
                    resolve(data);
                }).catch(error => {
                    reject(error);
                });
            });
        }
    }
};