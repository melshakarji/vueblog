// Default imports
import Vue from "vue";
import Vuex from "vuex";
import Axios from "axios";
import posts from "./modules/posts";
import auth from "./modules/auth";
import profile from "./modules/profile";
import comment from "./modules/comment";
import users from "./modules/users";
Vue.use(Vuex);



Vue.prototype.$http = Axios;
Axios.defaults.headers.common = {
    "X-Requested-With": "XMLHttpRequest"
};

let authentication = auth.state.data || null;
if (authentication != null) {
    Vue.prototype.$http.defaults.headers.common["Authorization"] =
        "Bearer " + authentication.access_token;
}





export default new Vuex.Store({
    modules: {
        posts,
        auth,
        profile,
        comment,
        users
    }
});