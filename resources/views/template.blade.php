<!DOCTYPE html>
<html>

<head>
    <!-- Metadata -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="language" content="{{ App::getLocale() }}">
    <title>Tempas</title>

    <!-- Stylesheets -->
    <link href=" {{ asset('css/app.css') }}" rel="stylesheet">

    <!-- Javascript -->

    <script>
        window.root = window.location.protocol + "//" + window.location.host;
    </script>
</head>

<body>
    <!-- Application -->
    <div id="app">
        <master></master>
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
</body>

</html>