<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:api', 'throttle: 100, 1')->group(function () {
    Route::post('addpost', 'PostController@store'); // add new post
    Route::put('addpost', 'PostController@store'); // add new post
    Route::post('post/react', 'PostController@react'); // add new post
    Route::delete('deletepost', 'PostController@delete'); // delete post
    
    Route::post('comment/add', 'CommentController@store'); // add new comment
    Route::delete('deletecomment', 'CommentController@delete'); // delete comment

    Route::post('update/user', 'UserController@store'); // update the user info
    Route::post('update/image', 'UserController@updateProfilePicture'); // update the user profile picture
    Route::post('update/password', 'UserController@updatepass'); // Update the users password
    Route::get('list/users', 'UserController@list'); // list all users for admin
    Route::delete('deleteuser', 'UserController@delete'); // list all users for admin
});

Route::get('posts/all', 'PostController@listPosts'); // list posts
Route::get('post/{id}', 'PostController@show'); // preview one post

Route::get('comment/list/{post}', 'CommentController@list'); // list comments for a post



