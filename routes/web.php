<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/{any}', 'AppController@index')->where('any', '^(?!api).*$')->name('app');

// Oauth routes
Route::post('/login', "TokenController@login");
Route::post('/register', "UserController@create");
Route::middleware('auth:api')->post('/logout', "TokenController@logout");